package main

import (
	"fmt"
	"strconv"
)

func newUnit(name string, terrains map[string]*terrain) unit {
	var unitB unit
	unitB.name = name
	unitB.terrain = make(map[string][]float64)
	unitB.counters = make(map[string]float64)


	for _, terrain := range terrains {
		unitB.terrain[terrain.name] = []float64 {0.0, 0.0, 0.0, 0.0}
	}
	unitB.canRecruit = true
	return unitB
}

func getBRString(br battleReport, short bool) string {
	retString1 := br.mainAAR.unit1name + " (" + strconv.Itoa(br.mainAAR.unit1startSize) + ") / " + br.mainAAR.unit2name + " (" + strconv.Itoa(br.mainAAR.unit2startSize) + ")"
	retString2 := "Main Phase: " + "\n" + getAARString(br.mainAAR)
	retString3 := "Pursuit 1-2: " + "\n" + getAARString(br.pursuit12AAR)
	retString4 := "Pursuit 2-1: " + "\n" + getAARString(br.pursuit21AAR)
	retString5 := "All Phases: Kill Ratio = " + fmt.Sprintf("%.2f", br.killRatio) + " Gold Ratio = " + fmt.Sprintf("%.2f", br.goldRatio)
	if short == false {
		return "\n" + retString2 + "\n" + retString3 + "\n" + retString4 + "\n" + retString5 + "\n\n"
	} else {
		return retString1 + "\n" + retString5
	}

}

func createBattleReport(mainAAR afterAction, pursuit12 afterAction, pursuit21 afterAction) battleReport {
	br := new(battleReport)
	br.mainAAR = mainAAR
	br.pursuit12AAR = pursuit12
	br.pursuit21AAR = pursuit21

	br.pursuit12weight = br.mainAAR.goldRatio / (1.0 + br.mainAAR.goldRatio)
	br.pursuit21weight = 1 - br.pursuit12weight

	br.weightedUnit2ManLosses = max(1.0,float64(br.mainAAR.unit2manLosses) + br.pursuit12weight * float64(br.pursuit12AAR.unit2manLosses) + br.pursuit21weight * float64(br.pursuit21AAR.unit2manLosses))
	br.weightedUnit1ManLosses = max(1.0,float64(br.mainAAR.unit1manLosses) + br.pursuit12weight * float64(br.pursuit12AAR.unit1manLosses) + br.pursuit21weight * float64(br.pursuit21AAR.unit1manLosses))

	br.weightedUnit2GoldLosses = max(1.0,float64(br.mainAAR.unit2goldLosses) + br.pursuit12weight * float64(br.pursuit12AAR.unit2goldLosses) + br.pursuit21weight * float64(br.pursuit21AAR.unit2goldLosses))
	br.weightedUnit1GoldLosses = max(1.0,float64(br.mainAAR.unit1goldLosses) + br.pursuit12weight * float64(br.pursuit12AAR.unit1goldLosses) + br.pursuit21weight * float64(br.pursuit21AAR.unit1goldLosses))

	br.killRatio = br.weightedUnit2ManLosses / br.weightedUnit1ManLosses
	br.goldRatio = br.weightedUnit2GoldLosses / br.weightedUnit1GoldLosses

	return *br

}

type battleReport struct {
	mainAAR afterAction
	pursuit12AAR afterAction
	pursuit21AAR afterAction

	pursuit12weight float64
	pursuit21weight float64
	killRatio float64
	goldRatio float64

	weightedUnit2ManLosses  float64
	weightedUnit1ManLosses  float64
	weightedUnit2GoldLosses float64
	weightedUnit1GoldLosses float64
}

func getAARString(aar afterAction) string {
	retString1 := aar.unit1name + " (" + strconv.Itoa(aar.unit1startSize) + ") / " + aar.unit2name + " (" + strconv.Itoa(aar.unit2startSize) + ")"
	retString2 := "Men Lost: " + strconv.Itoa(aar.unit1manLosses) + "/" + strconv.Itoa(aar.unit2manLosses)
	retString3 := "Value Destroyed: " + strconv.Itoa(aar.unit1goldLosses) + "/" + strconv.Itoa(aar.unit2goldLosses)
	retString4 := "Gold Ratio = " + fmt.Sprintf("%.2f", aar.goldRatio) + " Kill Ratio = " + fmt.Sprintf("%.2f", aar.killRatio)
	return retString1 + "\n" + retString2 + "\n" + retString3 + "\n" + retString4 + "\n"
}

func createAfterAction(unit1 unit, unit2 unit, terrain string) *afterAction {
	aar := new(afterAction)
	aar.unit1name = unit1.name
	aar.unit2name = unit2.name

	aar.unit1startSize = int(unit1.maxSize)
	aar.unit1endSize = int(unit1.currentSize)
	aar.unit2startSize = int(unit2.maxSize)
	aar.unit2endSize = int(unit2.currentSize)
	aar.terrain = terrain

	aar.unit1goldLosses = int((1.0 - (unit1.currentSize / unit1.maxSize) ) * goldBudget * hardCasualtyConversionRatio)
	aar.unit2goldLosses = int(( 1.0 - (unit2.currentSize / unit2.maxSize) ) * goldBudget * hardCasualtyConversionRatio)
	aar.unit1manLosses = int((unit1.maxSize - unit1.currentSize) * hardCasualtyConversionRatio)
	aar.unit2manLosses = int((unit2.maxSize - unit2.currentSize) * hardCasualtyConversionRatio)

	aar.goldRatio = float64(aar.unit2goldLosses) / max(1.0,float64(aar.unit1goldLosses))
	aar.killRatio = float64(aar.unit2manLosses) / max(1.0,float64(aar.unit1manLosses))

	aar.goldNet = aar.unit2goldLosses - aar.unit1goldLosses
	aar.killNet = aar.unit2manLosses - aar.unit1manLosses

	return aar
}

type afterAction struct {
	unit1name string
	unit2name string

	unit1startSize int
	unit2startSize int
	unit1endSize int
	unit2endSize int
	terrain string

	unit1startGold int
	unit2startGold int

	unit1endGold int
	unit2endGold int

	unit1goldLosses int
	unit2goldLosses int
	unit1manLosses int
	unit2manLosses int

	goldRatio float64
	killRatio float64

	goldNet int
	killNet int
}

func copyUnit(unitA unit) unit {
	var unitB unit
	unitB.name = unitA.name
	unitB.class = unitA.class

	unitB.damage = unitA.damage
	unitB.toughness = unitA.toughness
	unitB.pursuit = unitA.pursuit
	unitB.screen = unitA.screen
	unitB.regimentSize = unitA.regimentSize
	unitB.currentSize = unitA.currentSize
	unitB.maxSize = unitA.maxSize

	unitB.currentHP = unitA.currentHP
	unitB.maxHP = unitA.maxHP
	unitB.siege_value = unitA.siege_value
	unitB.siege_tier = unitA.siege_tier
	unitB.gold = unitA.gold
	unitB.gold_sv = unitA.gold_sv

	unitB.terrain = make(map[string][]float64)
	unitB.winter = make(map[string][]float64)
	unitB.counters = make(map[string]float64)
	for k, v := range unitA.counters {
		unitB.counters[k] = v
	}

	for k, v := range unitA.terrain {
		unitB.terrain[k] = v
	}

	for k, v := range unitA.winter {
		unitB.winter[k] = v
	}

	unitB.holyOrderFallback = unitA.holyOrderFallback
	unitB.mercenaryFallback = unitA.mercenaryFallback
	unitB.aiQuality = unitA.aiQuality
	unitB.icon = unitA.icon
	unitB.isCultural = unitA.isCultural
	unitB.isNoble = unitA.isNoble
	unitB.isLevy = unitA.isLevy
	unitB.isAdvanced = unitA.isAdvanced
	unitB.canRecruit = unitA.canRecruit

	unitB.finalGoldRatio = unitA.finalGoldRatio



	return unitB
}

// Data structure for unit type
type unit struct {

	// basic parameters
	name string
	class string

	// base stats
	damage float64
	toughness float64
	pursuit float64
	screen float64

	regimentSize float64

	currentSize float64
	maxSize float64
	currentHP float64
	maxHP float64

	siege_value float64
	siege_tier float64
	gold	float64
	gold_sv string

	counters map[string]float64
	terrain map[string][]float64
	winter map[string][]float64

	holyOrderFallback bool
	mercenaryFallback bool
	aiQuality float64
	icon string

	isCultural bool
	isNoble bool
	isAdvanced bool
	isLevy bool

	canRecruit bool

	finalGoldRatio float64

}
