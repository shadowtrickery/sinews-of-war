package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

type terrain struct {
	name string
	freq float64
	width float64
}

func getTerrainFreqs(file string, terrains map[string]*terrain) {

	provCount := 0.0
	thisFile, err := os.Open(file)
	if err != nil {
		fmt.Println("Failed to open file: " + file)
		log.Fatal(err)
	}
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)
	for scanner.Scan() {
		// get next line

		line := cleanLine(scanner.Text())
		fields := strings.Fields(line)
		//fmt.Println(fields)
		if len(fields) > 0 {
			subFields := strings.Split(fields[0], "=")

			if _, ok := terrains[subFields[1]]; ok {
				terrains[subFields[1]].freq++
				//fmt.Println("found terrain: \"" + subFields[1] +"\"")
				provCount++
			} else {
				fmt.Println("Unrecognized terrain: \"" + subFields[1] +"\"")
			}
			//provCount++
		}
	}
	for _, v := range terrains {
		v.freq = v.freq / provCount
	}
	//fmt.Println(provCount)

	fmt.Println("Found terrains:")
	for name, terrain := range terrains {
		fmt.Println("name = \"" + name + "\", width = " + fmt.Sprintf("%0.2f",terrain.width) + ", freq = " + fmt.Sprintf("%0.2f",terrain.freq))
	}

}

func getAllTerrainTypes(dir string) map[string]*terrain {
	terrains := make(map[string]*terrain)
	fileInfo, err := ioutil.ReadDir(dir)
	if err != nil {
		fmt.Println("failed to read directory: " + dir)
		log.Fatal(err)
	}

	for i := 0; i < len(fileInfo); i++ {
		if strings.Contains(fileInfo[i].Name(), "txt") {
			terrainInstances := getTerrainTypes(filepath.Join(dir, fileInfo[i].Name()))
			for k,v := range terrainInstances {
				terrains[k] = v
			}
		}

	}


	return terrains
}

func getTerrainTypes(file string) map[string]*terrain {
	thisFile, err := os.Open(file)
	if err != nil {
		fmt.Println("Failed to open file: " + file)
		log.Fatal(err)
	}
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)

	terrains := make(map[string]*terrain)
	currentTerrain := ""
	for scanner.Scan() {
		// get next line

		line := cleanLine(scanner.Text())
		fields := strings.Fields(line)
		if getIndentLevel(line) == 0 && !strings.Contains(line, "}") && len(strings.Fields(line)) > 0 {
			var newTerrain terrain
			reg, _ := regexp.Compile("[^a-zA-Z0-9_]+")
			fields[0] = reg.ReplaceAllString(fields[0], "")
			newTerrain.name = fields[0]
			newTerrain.freq = 0.0
			newTerrain.width = 1.0
			currentTerrain = newTerrain.name
			terrains[currentTerrain] = &newTerrain
		} else if strings.Contains(line, "combat_width") && len(strings.Fields(line)) > 2 {
			//fmt.Println(line)
			terrains[currentTerrain].width, err = strconv.ParseFloat(fields[2], 64)
			if err != nil {
				fmt.Println("failed to convert \"" + fields[2] + "\" to float")
			}
		}
	}

	return terrains
}

// removes comment blocks from a line (string)
func cleanLine(line string) string {
	// Look out for following byte signifying a comment
	commentByte := []byte("#")[0]
	// iterate through all bytes in line
	for i := 0; i < len(line); i++ {
		// if byte = comment byte, return line up until that byte
		if line[i] == commentByte {
			if i > 1 { return line[0:i] } else { return "" }
		}
	}
	// if no comments found, return whole line
	return line
}

func getIndentLevel(line string) int {
	i := 0
	for _, runeValue := range line {
		if runeValue == ' ' {
			i++
		} else if runeValue == []rune("\t")[0] {
			i+=4
		} else {
			break
		}
	}
	return i
}