﻿eliminate_raiders_cb = {
	group = conquest

	combine_into_one = yes
	should_show_war_goal_subview = yes
	mutually_exclusive_titles = { always = yes }

	allowed_for_character = {
	}

	allowed_against_character = {
		scope:attacker = {
			has_opinion_modifier = { target = scope:defender modifier = raided_me_opinion }
		}
	}
	target_titles = all
	target_title_tier = duchy
	target_de_jure_regions_above = yes
	ignore_effect = change_title_holder

	ai_can_target_all_titles = {
		
	}
	ai_score_mult = {
		value = viking_conquest_ai_score_value		
	}

	valid_to_start = {
		scope:target = {
			any_in_de_jure_hierarchy = {
				tier = tier_county
				neighboring_county_or_viking_conquest_trigger = { CHARACTER = root }
			}
		}
	}

	should_invalidate = {
		NOT = {
			any_in_list = {
				list = target_titles
				any_in_de_jure_hierarchy = {
					tier = tier_county
					holder = {
						target_is_same_character_or_above = scope:defender
					}
				}
			}
		}
	}

	on_invalidated_desc = msg_religious_war_invalidation_region_message
	
	on_invalidated = {
	}

	cost = {
		piety = {
			value = 0
		}
		prestige = {
			value = 0
		}
	}

	on_declaration = {
		on_declared_war = yes
	}

	on_victory_desc = {
		first_valid = {
			triggered_desc = {
				trigger = { scope:attacker = { is_local_player = yes } }
				desc = county_conquest_cb_victory_desc_attacker
			}
			desc = county_conquest_cb_victory_desc
		}
	}

	on_victory = {
		scope:attacker = { show_pow_release_message_effect = yes }
		create_title_and_vassal_change = {
			type = conquest
			save_scope_as = change
			add_claim_on_loss = yes
		}

		# go through the dejure hierarchy under target titles, transfer eligible vassals and sieze counties from ineligible ones
		every_in_list = {
			list = target_titles
			custom_tooltip = CONQUEST_CB_TITLE

			conquest_cb_title_transfer = {
				RELIGIOUS_WAR = no
			}
		}

		every_in_list = {
			list = vassals_taken
			change_liege = {
				liege = scope:attacker
				change = scope:change
			}
		}
		
		every_in_list = {
			list = titles_taken
			change_title_holder = {
				holder = scope:attacker
				change = scope:change
				take_baronies = yes
			}
		}

		resolve_title_and_vassal_change = scope:change

		# Prestige Progress for the Attacker
		every_in_list = {
			list = target_titles
			scope:attacker = {
				add_prestige_experience = medium_prestige_value
			}
		}

		# Prestige for the attacker's war allies
		add_from_contribution_attackers = {
			prestige = medium_prestige_value
			opinion = {
				modifier = contributed_in_war
			}
		}

		# Prestige for the defender's war allies
		add_from_contribution_defenders = {
			prestige = medium_prestige_value
			opinion = {
				modifier = contributed_in_war
			}
		}

		# Truce
		add_truce_attacker_victory_effect = yes

		# FP1: note the victory for future memorialisation via stele (if applicable).
		scope:attacker = { fp1_remember_recent_conquest_victory_effect = yes }
	}

	on_white_peace_desc = {
		first_valid = {
			triggered_desc = {
				trigger = { scope:defender = { is_local_player = yes } }
				desc = county_conquest_cb_white_peace_desc_defender
			}
			desc = county_conquest_cb_white_peace_desc
		}
	}

	on_white_peace = {
		scope:attacker = { show_pow_release_message_effect = yes }
		# Prestige loss for the attacker
		scope:attacker = {
			add_prestige = {
				value = minor_prestige_value
				multiply = -1.0
			}
		}

		# Prestige for the attacker's war allies
		add_from_contribution_attackers = {
			prestige = medium_prestige_value
			opinion = {
				modifier = contributed_in_war
			}
		}

		# Prestige for the defender's war allies
		add_from_contribution_defenders = {
			prestige = medium_prestige_value
			opinion = {
				modifier = contributed_in_war
			}
		}

		add_truce_white_peace_effect = yes
	}

	on_defeat_desc = {
		first_valid = {
			triggered_desc = {
				trigger = { scope:defender = { is_local_player = yes } }
				desc = county_conquest_cb_defeat_desc_defender
			}
			triggered_desc = {
				trigger = {
					scope:attacker = {
						is_local_player = yes
						has_targeting_faction = yes
					}
				}
				desc = county_conquest_cb_defeat_desc_attacker
			}
			desc = county_conquest_cb_defeat_desc
		}
	}

	on_defeat = {
		scope:attacker = { show_pow_release_message_effect = yes }

		# Prestige loss for the attacker
		scope:attacker = {
			pay_short_term_gold = {
				gold = 3
				target = scope:defender
				yearly_income = yes
			}
			add_prestige = {
				value = major_prestige_value
				multiply = -1.0
			}
		}

		# Prestige for Defender
		scope:defender = {
			add_prestige = major_prestige_value
		}

		# Prestige for the attacker's war allies
		add_from_contribution_attackers = {
			prestige = medium_prestige_value
			opinion = {
				modifier = contributed_in_war
			}
		}

		# Prestige for the defender's war allies
		add_from_contribution_defenders = {
			prestige = medium_prestige_value
			opinion = {
				modifier = contributed_in_war
			}
		}

		add_truce_attacker_defeat_effect = yes

		scope:attacker = {
			save_temporary_scope_as = loser
		}
		on_lost_aggression_war_discontent_loss = yes
	}

	transfer_behavior = transfer

	on_primary_attacker_death = inherit
	on_primary_defender_death = inherit

	attacker_allies_inherit = yes
	defender_allies_inherit = yes

	war_name = "ELIMINATE_RAIDERS_WAR_NAME"
	war_name_base = "ELIMINATE_RAIDERS_WAR_NAME_BASE"
	cb_name = "ELIMINATE_RAIDERS_CB_NAME"

	interface_priority = 79

	use_de_jure_wargoal_only = yes

	attacker_wargoal_percentage = 0.8
}