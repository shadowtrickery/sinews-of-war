﻿
demd_trade_resources = {
	alias = { demd_trade_resource }
	texture = "gfx/interface/icons/demd/resources/trade_resources/luxuries.png"
}

demd_gold = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/gold.png"
}

demd_silver = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/silver.png"
}

demd_wine = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/wine.png"
}

demd_silk = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/silk.png"
}

demd_spices = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/spices.dds"
}

demd_amber = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/amber.dds"
}

demd_gems = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/gems.dds"
}

demd_ivory = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/ivory.dds"
}

demd_salt = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/salt.dds"
}

demd_furs = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/furs.png"
}

demd_dyes = {
	parent = demd_trade_resources
	texture = "gfx/interface/icons/demd/resources/trade_resources/dyes.dds"
}
