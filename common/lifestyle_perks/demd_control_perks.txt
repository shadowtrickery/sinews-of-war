﻿serve_the_crown_perk = {
	lifestyle = martial_lifestyle
	tree = authority
	position = { 1 0 }
	icon = node_martial
	
	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_education_martial_trigger = yes
			}
			add = 1989
		}
		if = {
			limit = {
				has_focus = martial_authority_focus
			}
			multiply = 5
		}
		if = {
			limit = {
				can_start_new_lifestyle_tree_trigger = no
			}
			multiply = 0
		}
	}
	
	character_modifier = {
		monthly_county_control_change_add = 0.1
		dread_baseline_add = 15
	}
}
