﻿#On actions about titles


on_title_gain = {
	on_actions = { demd_title_gain }
}

demd_title_gain = {
		trigger = { 
			is_ai = yes 
			scope:title.tier = tier_county
		}
	effect = {
		scope:title = {
			# if player has fucked up edicts, unfuck them
			
			# reset to factory
			demd_set_edict_setting = { TYPE = tax LEVEL = 2 }
			demd_set_edict_setting = { TYPE = manpower LEVEL = 2 }
			demd_set_edict_setting = { TYPE = public_order LEVEL = 2 }
			demd_set_edict_setting = { TYPE = sanitation LEVEL = 2 }
			demd_set_edict_setting = { TYPE = irrigation LEVEL = 2 }
			demd_set_edict_setting = { TYPE = infrastructure LEVEL = 2 }
		
			# let AI make changes twice
			
			remove_variable = tax_setting_changed
			remove_variable = manpower_setting_changed
			remove_variable = public_order_setting_changed
			remove_variable = sanitation_setting_changed
			remove_variable = irrigation_setting_changed
			remove_variable = infrastructure_setting_changed
			
			setEdicts = yes
			
			remove_variable = tax_setting_changed
			remove_variable = manpower_setting_changed
			remove_variable = public_order_setting_changed
			remove_variable = sanitation_setting_changed
			remove_variable = irrigation_setting_changed
			remove_variable = infrastructure_setting_changed
			
			setEdicts = yes					
		}	
	}
}


