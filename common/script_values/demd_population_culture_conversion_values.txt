﻿#############################################
# DEMD Population System
# by Vertimnus
# Do not edit without making corresponding edits to the metascript source code. Bad things will happen!
#############################################

culture_conversion_acceptance_impact = {
	value = scope:county.var:population
	multiply = {
		value = -500
		divide = {
			value = 500
			add = scope:county.culture.var:culture_population
		}
	}
	scope:county.culture = {
		if = {
			limit = { has_same_culture_heritage = scope:councillor.culture }
			multiply = 0.67
		}			
	}
	max = 25
}

# Convert "full" percentage value to 0-1 scale
steward_progress_percentage = {
	value = steward_promote_culture_base_total
	divide = 100
}

#############################
# Progress Modifiers
#############################

steward_promote_culture_ai_weight = {
	value = steward_promote_culture_base_total
	add = scope:county.progress_from_county_culture_population
	add = scope:county.progress_from_neighboring_county_culture_population
	add = scope:county.progress_from_neighboring_councillor_culture_population
	add = steward_promote_culture_terrain_malus
	add = 100
	min = 0.001
}

steward_promote_culture_base_total = {
	value = scope:councillor.steward_skill
}

steward_skill = {
	value = stewardship
	divide = 20
}

progress_from_county_culture_population = {
	value = var:county_population
	multiply = culture.var:base_culture_power
	multiply = -0.0025	
}

progress_from_neighboring_county_culture_population = {
	value = 0
	every_in_list = {
		variable = county_neighbors
		limit = { culture = prev.culture }
		add = var:county_population
	}
	multiply = culture.var:base_culture_power
	multiply = 0.01
	multiply = -0.0625
}

progress_from_neighboring_councillor_culture_population = {
	value = 0
	every_in_list = {
		variable = county_neighbors
		limit = { culture = scope:councillor.culture }
		add = var:county_population
	}
	multiply = scope:councillor.culture.var:base_culture_power
	multiply = 0.00125
}

steward_promote_culture_terrain_malus = {
	value = scope:county.var:terrain_conversion_defense_bonus
	multiply = steward_promote_culture_base_total
}

steward_promote_culture_capital_bonus = {
	value = 0.25
	multiply = steward_promote_culture_base_total
}

steward_promote_culture_recent_rebellion_bonus = {
	value = steward_promote_culture_base_total
}

######################################################################################


#################
# Pop Factors
steward_promote_culture_pop_bonuses = {			
	# County Population
	add = {
		value = scope:county.progress_from_county_culture_population
		desc = STEWARD_CONVERSION_CULTURE_COUNTY
	}
	
	# Neighboring Councillor Faith Population
	add = {
		value = scope:county.progress_from_neighboring_councillor_culture_population
		desc = STEWARD_CONVERSION_CULTURE_NEIGHBORS_POS
	}
	
	# Neighboring County Faith Population
	add = {
		value = scope:county.progress_from_neighboring_county_culture_population
		desc = STEWARD_CONVERSION_CULTURE_NEIGHBORS_NEG
	}
	
	# From Terrain
	if = {
		limit = { scope:county.var:terrain_conversion_defense_bonus > 0 }
		add = {
			value = scope:councillor.steward_promote_culture_terrain_malus
			desc = STEWARD_PROMOTE_CULTURE_TERRAIN_MALUS
		}
	}
	
	# liege capital
	if = {
		limit = { scope:councillor_liege.capital_county = scope:county }
		add = {
			value = steward_promote_culture_capital_bonus
			desc = STEWARD_PROMOTE_CULTURE_CAPITAL_BONUS
		}
	}
	
	# Recent Rebellion
	if = {
		limit = { scope:county = { has_county_modifier = county_increased_opinion_modifier } }
		add = {
			value = scope:councillor.steward_promote_culture_recent_rebellion_bonus
			desc = county_increased_opinion_modifier
		}
	}
	
	# Same Faith Bonus
	if = {
		limit = {
			exists = scope:county
			scope:councillor_liege.faith = {
				has_doctrine_parameter = same_faith_promote_culture_bonus_active
			}
			scope:county.faith = scope:councillor_liege.faith
		}
		add = {
			value = scope:councillor.promote_culture_communal_identity_bonus
			desc = STEWARD_PROMOTE_CULTURE_COMMUNAL_IDENTITY_BONUS_MODIFIER
		}
	}
}

#################
# Perk Bonus
steward_promote_culture_perk_bonuses = {
	# Perk Bonuses
	if = {
		limit = {
			exists = scope:councillor_liege.dynasty
			scope:councillor_liege.dynasty = { has_dynasty_perk = erudition_legacy_5 }
		}
		add = {
			value = steward_promote_culture_erudition_bonus
			desc = ERUDITION_DYNASTY_PERK_BONUS_VALUE
		}
	}
}

#################
# Innovation Bonus
steward_promote_culture_innovation_bonuses = {
	if = {
		limit = {
			scope:councillor_liege.culture = {
				has_innovation = innovation_east_settling
			}
		}
		add = {
			value = scope:councillor.promote_culture_east_settling_bonus
			desc = STEWARD_PROMOTE_CULTURE_INNOVATION_BONUS_EAST_SETTLING
		}
	}
}
	
#################
#Various Modifier Penalties
steward_promote_culture_modifier_bonuses = {
	if = { #Reduced conversion speed if the county has protected traditions.
		limit = {
			exists = scope:county
			scope:county = {
				has_county_modifier = governance_1073_conversion_resistance_modifier
			}
		}
		add = {
			value = scope:councillor.promote_culture_local_traditions_respected_penalty
			desc = STEWARD_PROMOTE_CULTURE_LOCAL_TRADITIONS_RESPECTED
		}
	}
	if = {
		limit  = {
			exists = scope:county
			scope:county = {
				has_county_modifier = county_shuubiyya_modifier
			}
		}
		add = {
			value = scope:councillor.promote_culture_shuubiyya_penalty
			desc = STEWARD_PROMOTE_CULTURE_SHUUBIYYA
		}
	}
	if = {
		limit = {
			exists = scope:county
			scope:county = {
				OR = {
					has_county_modifier = enthusiastic_nomad_settlement_saharan_modifier
					has_county_modifier = nomad_settlement_saharan_modifier
				}
			}
		}
		add = {
			value = scope:councillor.promote_culture_allowed_nomad_settlement_penalty
			desc = STEWARD_PROMOTE_CULTURE_ALLOWED_NOMAD_SETTLEMENT
		}
	}
}

################################	
# Relation Bonuses/Penalties
steward_promote_culture_relationship_bonuses = {
	if = { # Friend
		limit = {
			scope:councillor_liege = {
				has_relation_friend = scope:councillor
				NOT = { has_relation_best_friend = scope:councillor }
			}
		}
		add = {
			value = scope:councillor.steward_promote_culture_monthly_increase_friend_bonus
			desc = COUNCILLOR_IS_YOUR_FRIEND
		}
	}
	if = { # Best Friend
		limit = {
			scope:councillor_liege = {
				has_relation_best_friend = scope:councillor
			}
		}
		add = {
			value = scope:councillor.steward_promote_culture_monthly_increase_best_friend_bonus
			desc = COUNCILLOR_IS_YOUR_BEST_FRIEND
		}
	}
	if = { # Rival
		limit = {
			scope:councillor_liege = {
				has_relation_rival = scope:councillor
				NOT = { has_relation_nemesis = scope:councillor }
			}
		}
		add = {
			value = scope:councillor.steward_promote_culture_monthly_increase_rival_bonus
			desc = COUNCILLOR_IS_YOUR_RIVAL
		}
	}
	if = { # Nemesis
		limit = {
			scope:councillor_liege = {
				has_relation_nemesis = scope:councillor
			}
		}
		add = {
			value = scope:councillor.steward_promote_culture_monthly_increase_nemesis_bonus
			desc = COUNCILLOR_IS_YOUR_NEMESIS
		}
	}
}

################################	
# Relation Bonuses/Penalties
steward_promote_culture_rule_bonuses = {
	# Conversion speed game rules
	if = {
		limit = {
			has_game_rule = slower_culture_conversion_speed
		}
		multiply = {
			value = slower_game_rule_value
			desc = GAME_RULE_SLOWER_REASON
		}
	}
	if = {
		limit = {
			has_game_rule = significantly_slower_culture_conversion_speed
		}
		multiply = {
			value = significantly_slower_game_rule_value
			desc = GAME_RULE_SIGNIFICANTLY_SLOWER_REASON
		}
	}
	if = {
		limit = {
			has_game_rule = significantly_faster_culture_conversion_speed
		}
		multiply = {
			value = significantly_faster_game_rule_value
			desc = GAME_RULE_SIGNIFICANTLY_FASTER_REASON
		}
	}
	if = {
		limit = {
			has_game_rule = faster_culture_conversion_speed
		}
		multiply = {
			value = faster_game_rule_value
			desc = GAME_RULE_FASTER_REASON
		}
	}
}