﻿#############################################
# DEMD Population System
# by Vertimnus
# Do not edit without making corresponding edits to the metascript source code. Bad things will happen!
#############################################



# Convert "full" percentage value to 0-1 scale
court_chaplain_progress_percentage = {
	value = court_chaplain_conversion_base_total
	divide = 100
}

#############################
# Progress Modifiers
#############################

court_chaplain_conversion_ai_weight = {
	value = court_chaplain_conversion_base_total
	add = scope:county.progress_from_county_faith_population
	add = scope:county.progress_from_neighboring_county_faith_population
	add = scope:county.progress_from_neighboring_councillor_faith_population
	add = court_chaplain_conversion_terrain_malus
	add = 100
	min = 0.001
}

court_chaplain_conversion_base_total = {
	value = scope:councillor.court_chaplain_skill
}

court_chaplain_skill = {
	value = learning
	divide = 20
}

progress_from_religious_icon = {
	value = learning
	divide = 20
}

progress_from_county_faith_population = {
	value = var:county_population	
	multiply = faith.fervor
	multiply = 0.01
	multiply = -0.005	
}

progress_from_neighboring_county_faith_population = {
	value = 0
	every_in_list = {
		variable = county_neighbors
		limit = { faith = prev.faith }
		add = var:county_population
	}
	multiply = faith.fervor
	multiply = 0.01
	multiply = -0.00125
}

progress_from_neighboring_councillor_faith_population = {
	value = 0
	every_in_list = {
		variable = county_neighbors
		limit = { faith = scope:councillor.faith }
		add = var:county_population
	}
	multiply = scope:councillor.faith.fervor
	multiply = 0.01
	multiply = 0.0025
}

court_chaplain_conversion_terrain_malus = {
	value = scope:county.var:terrain_conversion_defense_bonus
	multiply = court_chaplain_conversion_base_total
}

conversion_adaptive_resistance = {
	scope:county = {
		value = 0
		if = {
			limit = { faith = { has_doctrine_parameter = tenet_adaptive_conversion_resistance } }
			add = court_chaplain_conversion_base_total
			multiply = -0.3
		}
	}
}

conversion_unreformed_faith_penalty = {
	value = court_chaplain_conversion_base_total
	multiply = -0.3
}

conversion_holy_site_of_converter = {
	value = court_chaplain_conversion_base_total
	multiply = 0.3
}
conversion_holy_site_of_converted = {
	value = court_chaplain_conversion_base_total
	multiply = -0.3
}

court_chaplain_conversion_capital_bonus = {
	value = 0.25
	multiply = court_chaplain_conversion_base_total
}

court_chaplain_conversion_recent_rebellion_bonus = {
	value = court_chaplain_conversion_base_total
}

######################################################################################################

###################
# Pop Factors 
court_chaplain_conversion_pop_bonuses = {
	add = {
		value = scope:county.progress_from_county_faith_population
		desc = COURT_CHAPLAIN_CONVERSION_FERVOR_COUNTY
	}
	
	# Neighboring Councillor Faith Population
	add = {
		value = scope:county.progress_from_neighboring_councillor_faith_population
		desc = COURT_CHAPLAIN_CONVERSION_FERVOR_NEIGHBORS_POS
	}
	
	# Neighboring County Faith Population
	add = {
		value = scope:county.progress_from_neighboring_county_faith_population
		desc = COURT_CHAPLAIN_CONVERSION_FERVOR_NEIGHBORS_NEG
	}

	# From Terrain
	if = {
		limit = { scope:county.var:terrain_conversion_defense_bonus > 0 }
		add = {
			value = scope:councillor.court_chaplain_conversion_terrain_malus
			desc = COURT_CHAPLAIN_CONVERSION_TERRAIN_MALUS
		}
	}
	
	# liege capital
	if = {
		limit = { scope:councillor_liege.capital_county = scope:county }
		add = {
			value = court_chaplain_conversion_capital_bonus
			desc = COURT_CHAPLAIN_CONVERSION_CAPITAL_BONUS
		}
	}			

	# Recent Rebellion
	if = {
		limit = { scope:county = { has_county_modifier = county_increased_opinion_modifier } }
		add = {
			value = scope:councillor.court_chaplain_conversion_recent_rebellion_bonus
			desc = county_increased_opinion_modifier
		}
	}
	
	# Holy Site
	if = {
		limit = { scope:county.title_province.barony = { is_holy_site_of = scope:county.faith } }
		add = {
			value = scope:councillor.conversion_holy_site_of_converted
			desc = COURT_CHAPLAIN_CONVERSION_HOLY_SITE_OF_CONVERTED
		}
	}
	if = {
		limit = { scope:county.title_province.barony = { is_holy_site_of = scope:councillor.faith } }
		add = {
			value = scope:councillor.conversion_holy_site_of_converter
			desc = COURT_CHAPLAIN_CONVERSION_HOLY_SITE_OF_CONVERTER
		}
	}
	
	# Unreformed
	if = {
		limit = { scope:councillor.faith = { has_doctrine_parameter = unreformed } }
		add = {
			value = conversion_unreformed_faith_penalty
			desc = COURT_CHAPLAIN_CONVERSION_UNREFORMED_FAITH_PENALTY
		}
	}
}

#################
#Perk Bonuses
court_chaplain_conversion_perk_bonuses = {
	if = {
		limit = { scope:councillor_liege = { has_perk = zealous_proselytizer_perk } }
		add = {
			value = scope:councillor.court_chaplain_conversion_monthly_increase_perk_bonus
			desc = CONVERSION_PERK_BONUS_VALUE
		}
	}
	if = {
		limit = { scope:councillor_liege = { has_perk = religious_icon_perk } }
		add = scope:councillor_liege.progress_from_religious_icon
	}
	if = {
		limit = {
			scope:councillor_liege = {
				exists = dynasty
				dynasty = { has_dynasty_perk = erudition_legacy_5 }
			}
		}
		add = {
			value = scope:councillor.court_chaplain_conversion_monthly_increase_dynasty_perk_bonus
			desc = CONVERSION_DYNASTY_PERK_BONUS_VALUE
		}
	}
}

#################
# Relation Bonuseses
court_chaplain_conversion_relation_bonuses = {	
	if = { # Friend
		limit = {
			scope:councillor_liege = {
				has_relation_friend = scope:councillor
				NOT = { has_relation_best_friend = scope:councillor }
			}
		}
		add = {
			value = scope:councillor.court_chaplain_conversion_monthly_increase_friend_bonus
			desc = COUNCILLOR_IS_YOUR_FRIEND
		}
	}
	if = { # Best Friend
		limit = { scope:councillor_liege = { has_relation_best_friend = scope:councillor } }
		add = {
			value = scope:councillor.court_chaplain_conversion_monthly_increase_best_friend_bonus
			desc = COUNCILLOR_IS_YOUR_BEST_FRIEND
		}
	}
	if = { # Rival
		limit = {
			scope:councillor_liege = {
				has_relation_rival = scope:councillor
				NOT = { has_relation_nemesis = scope:councillor }
			}
		}
		add = {
			value = scope:councillor.court_chaplain_conversion_monthly_increase_rival_bonus
			desc = COUNCILLOR_IS_YOUR_RIVAL
		}
	}
	if = { # Nemesis
		limit = { scope:councillor_liege = { has_relation_nemesis = scope:councillor } }
		add = {
			value = scope:councillor.court_chaplain_conversion_monthly_increase_nemesis_bonus
			desc = COUNCILLOR_IS_YOUR_NEMESIS
		}
	}
}

#################
#Conversion penalties for County Faith Tenets
court_chaplain_conversion_county_tenet_bonuses = {
	if = {
		limit = { scope:county.faith = { has_doctrine = tenet_false_conversion_sanction } }
		add = {
			value = scope:councillor.conversion_false_conversion_sanction_resistance
			desc = COURT_CHAPLAIN_CONVERSION_FALSE_CONVERSION_MODIFIER
		}
	}
	if = {
		limit = { scope:county.faith = { has_doctrine = tenet_reincarnation } }
		add = {
			value = scope:councillor.conversion_reincarnation_resistance
			desc = COURT_CHAPLAIN_CONVERSION_REINCARNATION_MODIFIER
		}
	}
	if = {
		limit = { scope:county.faith = { has_doctrine = tenet_dharmic_pacifism } }
		add = {
			value = scope:councillor.conversion_dharmic_pacifism_resistance
			desc = COURT_CHAPLAIN_CONVERSION_DHARMIC_PACIFISM_MODIFIER
		}
	}
	if = {
		limit = { scope:county.faith = { has_doctrine_parameter = tenet_adaptive_conversion_resistance } }
		add = {
			value = scope:councillor.conversion_adaptive_resistance
			desc = COURT_CHAPLAIN_CONVERSION_ADAPTIVE_MODIFIER
		}
	}
	if = {
		limit = { scope:county.faith = { has_doctrine = tenet_pastoral_isolation } }
		add = {
			value = scope:councillor.conversion_pastoral_isolation_resistance
			desc = COURT_CHAPLAIN_CONVERSION_PASTORAL_ISOLATION_MODIFIER
		}
	}
	if = {
		limit = {
			scope:councillor.liege.faith = { has_doctrine = tenet_communal_identity }
			NOT = { scope:county.culture = scope:councillor.liege.culture }
		}
		add = {
			value = scope:councillor.conversion_communal_identity_resistance
			desc = COURT_CHAPLAIN_CONVERSION_COMMUNAL_IDENTITY_PENALTY_MODIFIER
		}
	}
}

#################
#Conversion penalties for Character Faith Tenets
court_chaplain_conversion_character_tenet_bonuses = {
	if = {
		limit = { scope:councillor.faith = { has_doctrine_parameter = mendicant_preachers_conversion_active } }
		add = {
			value = scope:councillor.conversion_mendicant_preachers_bonus
			desc = COURT_CHAPLAIN_CONVERSION_MENDICANT_PREACHERS_MODIFIER
		}
	}
	if = {
		limit = { 
			scope:councillor.faith = { has_doctrine_parameter = same_culture_conversion_bonus_active }
			scope:county.culture = scope:councillor.liege.culture
		}
		add = {
			value = scope:councillor.conversion_communal_identity_bonus
			desc = COURT_CHAPLAIN_CONVERSION_COMMUNAL_IDENTITY_BONUS_MODIFIER
		}
	}
	if = {
		limit = {
			scope:councillor.faith = {
				has_doctrine_parameter = ghw_no_hof_conversion_buffs_active
				OR = {
					has_doctrine_parameter = no_head_of_faith
					NOT = { exists = religious_head }
				}
				holy_sites_controlled >= 1
			}
		}
		add = {
			value = scope:councillor.conversion_ghw_tenet_no_hof_bonus
			desc = COURT_CHAPLAIN_CONVERSION_GHW_TENET_NO_HOF_MODIFIER
		}
	}
}

#################
#Maluses for syncretic faiths
court_chaplain_conversion_syncretic_tenet_bonuses = {
	if = {
		limit = {
			OR = {
				AND = {
					scope:councillor.faith = { has_doctrine_parameter = unreformed_syncretic_actor_opinion_active }
					scope:county.faith = { has_doctrine = unreformed_faith_doctrine}
				}
				AND = {
					scope:councillor.faith = { has_doctrine_parameter = christian_syncretic_actor_opinion_active }
					scope:county.faith = { religion_tag = christianity_religion }
				}
				AND = {
					scope:councillor.faith = { has_doctrine_parameter = islamic_syncretic_actor_opinion_active }
					scope:county.faith = { religion_tag = islam_religion }
				}
				AND = {
					scope:councillor.faith = { has_doctrine_parameter = jewish_syncretic_actor_opinion_active }
					scope:county.faith = { religion_tag = judaism_religion }
				}
			}
		}
		add = {
			value = scope:councillor.conversion_syncretic_with_religion_malus
			desc = COURT_CHAPLAIN_CONVERSION_SYNCRETIC_WITH_RELIGION_MODIFIER
		}
	}
}

#################
#Various Modifier Penalties
court_chaplain_conversion_modifier_bonuses = {
	if = {
		limit = { scope:county = { has_county_modifier = governance_1074_local_faith_respected_modifier } }
		add = {
			value = scope:councillor.local_faith_respected_modifier_value
			desc = COURT_CHAPLAIN_CONVERSION_LOCAL_COUNTY_MODIFIERS
		}
	}

	if = {
		limit = { scope:county = { has_county_modifier = governance_1074_population_fled_persecution_modifier } }
		add = {
			value = scope:councillor.population_fled_persecution_modifier_value
			desc = COURT_CHAPLAIN_CONVERSION_LOCAL_COUNTY_MODIFIERS
		}
	}
	if = {
		limit = {
			scope:councillor.liege = {
				OR = {
					has_character_modifier = defiant_high_king_conversion_boost_modifier
					has_character_modifier = defiant_high_queen_conversion_boost_modifier
				}
				religion = religion:germanic_religion
			}
			scope:county.faith = {
				NOT = { has_doctrine = unreformed_faith_doctrine }
			}
		}
		add = {
			value = scope:councillor.defiant_high_monarch_conversion_boost_value
			desc = COURT_CHAPLAIN_CONVERSION_LIEGE_IS_DEFIANT_HIGH_MONARCH
		}
	}
	if = {
		limit = { scope:county = { has_variable = learning_encouraged_conversion } }
		add = {
			value = scope:councillor.conversion_learning_theology
			desc = COURT_CHAPLAIN_CONVERSION_LEARNING_THEOLOGY
		}
	}
}

#################
# Traits

court_chaplain_conversion_trait_bonuses = {
	if = {
		limit = { scope:councillor.liege = { has_trait = savior } }
		add = {
			value = scope:councillor.conversion_savior_liege
			desc = COURT_CHAPLAIN_CONVERSION_LIEGE_IS_SAVIOR
		}
	}
	if = {
		limit = { scope:councillor.liege = { has_trait = divine_blood } }
		add = {
			value = scope:councillor.conversion_divine_blood_liege
			desc = COURT_CHAPLAIN_CONVERSION_LIEGE_IS_DIVINE_BLOOD
		}
	}
}

#################
# Rules

court_chaplain_conversion_rule_bonuses = {
	# Conversion speed game rules
	if = {
		limit = {
			has_game_rule = slower_faith_conversion_speed
		}
		multiply = {
			value = slower_game_rule_value
			desc = GAME_RULE_SLOWER_REASON
		}
	}
	if = {
		limit = {
			has_game_rule = significantly_slower_faith_conversion_speed
		}
		multiply = {
			value = significantly_slower_game_rule_value
			desc = GAME_RULE_SIGNIFICANTLY_SLOWER_REASON
		}
	}
	if = {
		limit = {
			has_game_rule = faster_faith_conversion_speed
		}
		multiply = {
			value = faster_game_rule_value
			desc = GAME_RULE_FASTER_REASON
		}
	}
	if = {
		limit = {
			has_game_rule = significantly_faster_faith_conversion_speed
		}
		multiply = {
			value = significantly_faster_game_rule_value
			desc = GAME_RULE_SIGNIFICANTLY_FASTER_REASON
		}
	}
}