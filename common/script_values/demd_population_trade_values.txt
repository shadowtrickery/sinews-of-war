﻿#############################################
# DEMD Population System
# by Vertimnus
# Do not edit without making corresponding edits to the metascript source code. Bad things will happen!
#############################################

node_produced_trade_value = {
	value = 0
	every_in_list = {
		variable = node_counties
		add = var:produced_trade_value
	}
}



node_trade_power = {
	value = 0
	every_in_list = {
		variable = node_counties
		add = var:trade_power
	}
}

node_attack_strength = {
	value = 0
	every_in_list = {
		variable = trade_node_raiders
		add = max_military_strength
	}
}

node_defense_strength = {
	value = var:trade_node_owner.top_liege.max_military_strength
	if = {
		limit = { NOT = { var:trade_node_owner.top_liege = var:trade_node_owner } }
		add = var:trade_node_owner.max_military_strength
	}
}

node_trade_efficiency = {
	value = var:node_defense_strength
	subtract = {
		value = var:node_attack_strength
		divide = 4
	}
	divide = var:node_defense_strength
}