﻿namespace = demd_pop_debug

# check if any counties are not in a node
demd_pop_debug.1001 = {
	hidden = yes
	type = empty
	immediate = {
		every_county = {
			limit = { NOT = { has_variable = trade_node } }
			county = { save_scope_as = problem_county }
			every_player = {
				send_interface_message = {
					type = event_court_chaplain_task_bad
					title = demd_debug_node
					desc = demd_debug_node
					right_icon = scope:problem_county
				}
			}
		}
	}
}

# print empire pops
demd_pop_debug.1002 = {
	hidden = yes
	type = empty
	immediate = {
		every_empire = { 
			save_scope_as = empire
			every_player = {
				send_interface_message = {
					type = event_court_chaplain_task_good
					title = demd_debug_empire
					desc = demd_debug_empire
					right_icon = scope:empire
				}
			}
		}

	}
}

# check which cultures aren't registered in the list
demd_pop_debug.1003 = {
	hidden = yes
	type = empty
	immediate = {
		every_county = {
			county = { save_scope_as = problem_county }
			if = {
				limit = { NOT = { culture = { has_variable = food_districts_cultural_production_mult } } }
				every_player = {
					send_interface_message = {
						type = event_court_chaplain_task_bad
						title = demd_debug_culture
						desc = demd_debug_culture
						right_icon = scope:problem_county
					}
				}
			}
		}
	}
}

# check which provinces are in the list but have no owner
demd_pop_debug.1004 = {
	hidden = yes
	type = empty
	immediate = {
		every_county = {
			save_scope_as = problem_county 
			holder = { save_scope_as = owner }
			if = {
				limit = { NOT = { exists = holder.liege.capital_province } } 
				every_player = {
					send_interface_message = {
						type = event_court_chaplain_task_bad
						title = demd_debug_owner
						desc = demd_debug_owner
						right_icon = scope:problem_county
						left_icon = scope:owner
					}
				}
			}
		}
	}
}


