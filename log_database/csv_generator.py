def pop_extract():
    with open('debug.log','r+',encoding='utf-8-sig') as debug_file:
        text = debug_file.readlines()
        data = ['Year;County;Duchy;Kingdom;Empire;pop;urban_pop;food_districts;good_districts;growth_rate;gross_tax;feature_upkeep;sewer_upkeep;barracks_upkeep;diesase_death_rate;culture_conversion_progress;faith_conversion_progress;county_maa;control_limit\n']
        for line in text:
            if 'common/on_action/yearly_on_actions/demd_debug_on_actions.txt line: 9:' in line:
                data.append(line.split('line: 9: ')[1])
    print('Log file read')
    with open('pop_data.csv','w+',encoding='utf-8-sig') as csv_file:
        for row in data :
            csv_file.write(row)
    print('pop_data.csv written')

def node_extract():
    with open('debug.log','r+',encoding='utf-8-sig') as debug_file:
        text = debug_file.readlines()
        data = ['Year;Node;Outgoing Trade;Incoming Trade\n']
        for line in text:
            if 'common/on_action/yearly_on_actions/demd_debug_on_actions.txt line: 14: ' in line:
                data.append(line.split('line: 14: ')[1])
    print('Log file read')
    with open('trade_data.csv','w+',encoding='utf-8-sig') as csv_file:
        for row in data :
            csv_file.write(row)
    print('trade_data.csv written')
if __name__ == '__main__' :
    pop_extract()
    node_extract()
