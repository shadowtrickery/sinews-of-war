rulerSetup = {
	every_count_or_above = {
		
	}
}

setKnightCapacity = {
	every_count_or_above = {
		set_variable = { name = temp value = ruler_knight_capacity }

		remove_all_character_modifier_instances = demd_char_knight_capacity_modifier_big
		remove_all_character_modifier_instances = demd_char_knight_capacity_modifier_medium
		remove_all_character_modifier_instances = demd_char_knight_capacity_modifier_small

		while = {
			limit = { var:temp > 25 }
			add_character_modifier = demd_char_knight_capacity_modifier_big
			change_variable = { name = temp subtract = 25 }
		}
		while = {
			limit = { var:temp > 5 }
			add_character_modifier = demd_char_knight_capacity_modifier_medium
			change_variable = { name = temp subtract = 5 }
		}
		while = {
			limit = { var:temp > 1 }
			add_character_modifier = demd_char_knight_capacity_modifier_small
			change_variable = { name = temp subtract = 1 }
		}
	}
}

setRegimentCapacity = {
	every_count_or_above = {
		set_variable = {
			name = ruler_manpower
			value = ruler_manpower
		}
		set_variable = {
			name = regiment_capacity
			value = total_regiment_capacity
		}
		set_variable = { name = temp value = var:regiment_capacity }
		
		remove_all_character_modifier_instances = demd_char_regiment_capacity_modifier_big
		remove_all_character_modifier_instances = demd_char_regiment_capacity_modifier_medium
		remove_all_character_modifier_instances = demd_char_regiment_capacity_modifier_small

		while = {
			limit = { var:temp > 25 }
			add_character_modifier = demd_char_regiment_capacity_modifier_big
			change_variable = { name = temp subtract = 25 }
		}
		while = {
			limit = { var:temp > 5 }
			add_character_modifier = demd_char_regiment_capacity_modifier_medium
			change_variable = { name = temp subtract = 5 }
		}
		while = {
			limit = { var:temp > 1 }
			add_character_modifier = demd_char_regiment_capacity_modifier_small
			change_variable = { name = temp subtract = 1 }
		}
	}
}






