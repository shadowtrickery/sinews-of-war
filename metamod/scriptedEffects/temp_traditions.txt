﻿#############################################
# DEMD Population System
# by Vertimnus
# This file was compiled by a machine from jomini metascript source code.
# It should never be manually edited.
#############################################

demd_add_tradition_bonuses = {

	if = {
		limit = { has_cultural_tradition = tradition_only_the_strong }
		change_variable = { name = manpower_cultural_production_mult add = -0.20 }
	}

	if = {
		limit = { has_cultural_tradition = tradition_strength_in_numbers }
		change_variable = { name = manpower_cultural_production_mult add = 0.15 }
	}

	if = {
		limit = { has_cultural_tradition = tradition_strength_in_numbers }
		change_variable = { name = manpower_cultural_production_mult add = 0.15 }
	}

	if = {
		limit = { has_cultural_tradition = tradition_legalistic }
		change_variable = { name = public_order_cultural_production_mult add = 0.10 }
	}

	if = {
		limit = { has_cultural_tradition = tradition_agrarian }
		change_variable = { name = food_cultural_production_mult add = 0.10 }
	}

	if = {
		limit = { has_cultural_tradition = tradition_caravaneers }
		change_variable = { name = trade_power_cultural_production_mult add = 0.10 }
	}

}