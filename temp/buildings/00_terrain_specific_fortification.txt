﻿### outposts

outposts_01 = {
	construction_time = quick_construction_time

	can_construct_potential = {
		always = no
		building_requirement_castle_city_church = { LEVEL = 01 }
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = cheap_building_tier_1_cost
	
	next_building = outposts_02

	type_icon = "icon_building_outposts.dds"
	
	ai_value = {
		base = 10
		ai_general_building_modifier = yes
		modifier = {
			factor = 0
			building_barracks_requirement_terrain = yes
			NOT = {
				has_building_or_higher = barracks_01
			}
		}
		modifier = {
			factor = 0
			building_camel_farms_requirement_terrain = yes
			NOT = {
				has_building_or_higher = camel_farms_01
			}
		}
		modifier = {
			add = -9
			has_building_or_higher = city_01
		}
	}
	
	on_complete = { reset_building_vars = yes }
}

outposts_02 = {
	construction_time = quick_construction_time

	can_construct_potential = {
		building_requirement_castle_city_church = { LEVEL = 01 }
		culture = {
			has_cultural_era_or_later = culture_era_tribal
		}
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = cheap_building_tier_2_cost
	
	county_modifier = {
		monthly_county_control_change_add = 0.1
	}
	
	next_building = outposts_03
	
	ai_value = {
		base = 9
		ai_general_building_modifier = yes
	}
	
	flag = demd_public_order_building_02
}

outposts_03 = {
	construction_time = quick_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 03 }
		culture = {
			has_cultural_era_or_later = culture_era_early_medieval
		}
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = cheap_building_tier_3_cost
	
	county_modifier = {
		monthly_county_control_change_add = 0.1
	}
	
	next_building = outposts_04
	
	ai_value = {
		base = 8
		ai_general_building_modifier = yes
	}
	
	flag = demd_public_order_building_03
}

outposts_04 = {
	construction_time = quick_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 04 }
		culture = {
			has_cultural_era_or_later = culture_era_early_medieval
		}
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = cheap_building_tier_4_cost
	
	county_modifier = {
		monthly_county_control_change_add = 0.1
	}
	
	next_building = outposts_05
	
	ai_value = {
		base = 7
		ai_general_building_modifier = yes
	}
	
	flag = demd_public_order_building_04
}

outposts_05 = {
	construction_time = quick_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 05 }
		culture = {
			has_cultural_era_or_later = culture_era_high_medieval
		}
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = cheap_building_tier_5_cost
	
	county_modifier = {
		monthly_county_control_change_add = 0.1
	}
	
	next_building = outposts_06
	
	ai_value = {
		base = 6
		ai_general_building_modifier = yes
	}
	
	flag = demd_public_order_building_05
}

outposts_06 = {
	construction_time = quick_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 06 }
		culture = {
			has_cultural_era_or_later = culture_era_high_medieval
		}
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = cheap_building_tier_6_cost
	
	county_modifier = {
		monthly_county_control_change_add = 0.1
	}
		
	next_building = outposts_07
	
	ai_value = {
		base = 5
		ai_general_building_modifier = yes
	}
	
	flag = demd_public_order_building_06
}

outposts_07 = {
	construction_time = quick_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 07 }
		culture = {
			has_cultural_era_or_later = culture_era_late_medieval
		}
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = cheap_building_tier_7_cost
	
	county_modifier = {
		monthly_county_control_change_add = 0.1
	}

	next_building = outposts_08
	
	ai_value = {
		base = 4
		ai_general_building_modifier = yes
	}
	
	flag = demd_public_order_building_07
}

outposts_08 = {
	construction_time = quick_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 08 }
		culture = {
			has_cultural_era_or_later = culture_era_late_medieval
		}
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = cheap_building_tier_8_cost
	
	county_modifier = {
		monthly_county_control_change_factor = 0.15
		monthly_county_control_change_add = 0.1
	}
	
	ai_value = {
		base = 3
		ai_general_building_modifier = yes
	}
	
	flag = demd_public_order_building_08
}