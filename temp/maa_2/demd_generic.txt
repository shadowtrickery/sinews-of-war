﻿# standard costs
@maa_buy_cost = 150
@maa_low_maintenance_cost = 1.0
@maa_high_maintenance_cost = 5.0

###
light_footmen = {
	type = skirmishers
	
	damage = 18
	toughness = 18
	pursuit = 0
	screen = 0
	
	terrain_bonus = { }

	counters = { }

	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	mercenary_fallback = no
	holy_order_fallback = no
	
	stack = 80
	ai_quality = { value = culture_ai_weight_skirmishers }
	icon = skirmishers
}

###
bowmen = {
	type = archers
	
	damage = 36
	toughness = 18
	pursuit = 0
	screen = 0
	
	terrain_bonus = {
		hills = { toughness = 5 }
		mountains = { toughness = 5 }
		desert_mountains = { toughness = 5 }
		forest = { toughness = 5 }
		taiga = { toughness = 5 }
		jungle = { toughness = 5 }
	}

	counters = {
		light_cavalry = 1
		elephant_cavalry = 1		
	}

	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	mercenary_fallback = yes
	holy_order_fallback = yes
	
	stack = 60
	ai_quality = { value = culture_ai_weight_archers }
}

###
light_horsemen = {
	type = heavy_cavalry

	damage = 30
	toughness = 20
	pursuit = 18
	screen = 18
	
	terrain_bonus = {
		farmlands = { damage = 5 pursuit = 5 screen = 5 }
		plains = { damage = 5 pursuit = 5 screen = 5 }
		drylands = { damage = 5 pursuit = 5 screen = 5 }
		steppe = { damage = 5 pursuit = 5 screen = 5 }
		hills = { damage = -5 pursuit = -5 screen = -5 }
		forest = { damage = -5 pursuit = -5 screen = -5 }
		taiga = { damage = -5 pursuit = -5 screen = -5 }
		mountains = { damage = -10 pursuit = -10 screen = -10 }
		desert_mountains = { damage = -10 pursuit = -10 screen = -10 }
		wetlands = { damage = -10 pursuit = -10 screen = -10 }
		jungle = { damage = -10 pursuit = -10 screen = -10 }
	}
	
	counters = {
		archers = 1
	}

	winter_bonus = {
		harsh_winter = { damage = -5 toughness = -2 }
	}

	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	mercenary_fallback = yes
	holy_order_fallback = yes
	
	stack = 45
	ai_quality = { value = culture_ai_weight_light_cavalry }
	icon = light_cavalry
}

###
pikemen_unit = {
	type = pikemen
	
	damage = 22
	toughness = 22
	pursuit = 0
	screen = 12
	
	terrain_bonus = {
		hills = { toughness = 3 screen = 3 }
		mountains = { toughness = 3 screen = 3 }
		desert_mountains = { toughness = 3 screen = 3 }
		forest = { toughness = -2 screen = -2 }
		taiga = { toughness = -3 screen = -3 }
		jungle = { toughness = -3 screen = -3 }
	}

	counters = {
		heavy_cavalry = 1
		elephant_cavalry = 1
	}

	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	mercenary_fallback = yes
	holy_order_fallback = yes
	
	stack = 60
	ai_quality = { value = culture_ai_weight_pikemen }
	icon = pikemen
}

###
armored_footmen = {
	type = heavy_infantry
	
	damage = 22
	toughness = 22
	pursuit = 10
	screen = 10
	
	counters = { }
	
	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	mercenary_fallback = yes
	holy_order_fallback = yes
	
	stack = 60
	ai_quality = { value = culture_ai_weight_heavy_infantry }
	icon = heavy_infantry
}

###
armored_horsemen = {
	type = heavy_cavalry
	
	damage = 50
	toughness = 30
	pursuit = 22
	screen = 18
	
	terrain_bonus = {
		farmlands = { damage = 5 pursuit = 5 screen = 5 }
		plains = { damage = 5 pursuit = 5 screen = 5 }
		drylands = { damage = 5 pursuit = 5 screen = 5 }
		steppe = { damage = 5 pursuit = 5 screen = 5 }
		hills = { damage = -5 pursuit = -5 screen = -5 }
		forest = { damage = -5 pursuit = -5 screen = -5 }
		taiga = { damage = -5 pursuit = -5 screen = -5 }
		jungle = { damage = -10 pursuit = -10 screen = -10 }
		mountains = { damage = -15 pursuit = -10 screen = -10 }
		desert_mountains = { damage = -15 pursuit = -10 screen = -10 }
		wetlands = { damage = -15 pursuit = -10 screen = -10 }
	}

	counters = {
		archers = 1
	}

	winter_bonus = {
		normal_winter = { damage = -10 toughness = -5 }
		harsh_winter = { damage = -20 toughness = -10 }
	}

	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	stack = 30
	ai_quality = { value = culture_ai_weight_heavy_cavalry }
	icon = heavy_cavalry
	fallback_in_hired_troops_if_unlocked = yes
}

###
crossbowmen = {
	type = archers
	
	damage = 40
	toughness = 20
	pursuit = 0
	screen = 0
	
	terrain_bonus = {
		hills = { toughness = 5 }
		mountains = { toughness = 5 }
		desert_mountains = { toughness = 5 }
		forest = { toughness = 5 }
		taiga = { toughness = 5 }
		jungle = { toughness = 5 }
	}

	counters = {
		light_cavalry = 1
		elephant_cavalry = 1		
	}

	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	holy_order_fallback = yes
	
	stack = 60
}

###
horse_archers = {
	type = archer_cavalry
	
	damage = 36
	toughness = 14
	pursuit = 30
	screen = 15

	terrain_bonus = {
		steppe = { damage = 10 pursuit = 10 screen = 10 }
		farmlands = { damage = 5 pursuit = 5 screen = 5 }
		plains = { damage = 5 pursuit = 5 screen = 5 }
		drylands = { damage = 5 pursuit = 5 screen = 5 }
		hills = { damage = -5 pursuit = -5 screen = -5 }
		forest = { damage = -5 pursuit = -5 screen = -5 }
		taiga = { damage = -5 pursuit = -5 screen = -5 }
		jungle = { damage = -10 pursuit = -10 screen = -10 }
		mountains = { damage = -15 pursuit = -15 screen = -15 }
		desert_mountains = { damage = -15 pursuit = -15 screen = -15 }
		wetlands = { damage = -15 pursuit = -15 screen = -15 }
	}

	counters = {
		pikemen = 1
	}

	can_recruit = {
		culture = { has_cultural_parameter = unlock_maa_horse_archers }
	}

	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	stack = 45
	ai_quality = { value = @cultural_maa_extra_ai_score }
	icon = horse_archers
}

###
war_elephant = {
	type = elephant_cavalry
	
	damage = 100
	toughness = 70
	pursuit = 20
	screen = 0
	
	siege_value = 0.1
	
	terrain_bonus = {
		hills = { damage = -20 pursuit = -5 }
		mountains = { damage = -40 pursuit = -10 }
		desert_mountains = { damage = -40 pursuit = -10 }
		wetlands = { damage = -40 pursuit = -10 }
	}
	
	counters = {
		heavy_infantry = 1
	}

	winter_bonus = {
		normal_winter = { damage = -30 toughness = -5 }
		harsh_winter = { damage = -60 toughness = -10 }
	}

	buy_cost = { gold = demd_maa_recruitment }	
	low_maintenance_cost = { gold = demd_unraised_maa_maintenance }
	high_maintenance_cost = { gold = demd_raised_maa_maintenance }
	
	stack = 15
	hired_stack_size = 10
	ai_quality = { value = culture_ai_weight_elephants }
	icon = war_elephants
}

